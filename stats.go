package fishbans

import "fmt"

// BanStats holds data for stats on bans
type BanStats struct {
	Success bool `json:"success"`
	Stats   struct {
		Username  string         `json:"username"`
		UUID      string         `json:"uuid"`
		TotalBans int            `json:"totalbans"`
		Service   map[string]int `json:"service"`
	} `json:"stats"`
}

// GetBanStats gets stats on bans for a user
func (c *Client) GetBanStats(user string) (stats *BanStats, err error) {
	if user == "" {
		err = ErrUserRequired
		return
	}

	stats = &BanStats{}
	err = c.getAndUnmarshal(fmt.Sprintf(BanStatsEndpoint, user), stats)

	if !stats.Success {
		err = ErrUsernameNotFound
		return
	}
	return
}

// GetServiceBanStats gets stats on bans for a user on a specific ban service
func (c *Client) GetServiceBanStats(user, service string) (stats *BanStats, err error) {
	if user == "" {
		err = ErrUserRequired
		return
	}

	if service == "" {
		err = ErrServiceRequired
		return
	}

	stats = &BanStats{}
	err = c.getAndUnmarshal(fmt.Sprintf(ServiceBanStatsEndpoint, user, service), stats)

	if !stats.Success {
		err = ErrUsernameNotFound
		return
	}
	return
}
