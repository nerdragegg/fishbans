package fishbans

import "fmt"

// Bans holds ban information for all services
type Bans struct {
	Success bool `json:"success"`
	Bans    struct {
		Username string `json:"username"`
		UUID     string `json:"uuid"`
		Service  struct {
			MCBans    *ServiceBan `json:"mcbans,omitempty"`
			Mcbouncer *ServiceBan `json:"mcbouncer,omitempty"`
			Mcblockit *ServiceBan `json:"mcblockit,omitempty"`
			Minebans  *ServiceBan `json:"minebans,omitempty"`
			Glizer    *ServiceBan `json:"glizer,omitempty"`
		} `json:"service"`
	} `json:"bans"`
}

// ServiceBan holds information about bans for a service
type ServiceBan struct {
	Bans    int               `json:"bans,omitempty"`
	BanInfo map[string]string `json:"ban_info,omitempty"`
}

// GetBans gets bans for a user on all services
func (c *Client) GetBans(user string) (bans *Bans, err error) {
	if user == "" {
		err = ErrUserRequired
		return
	}

	bans = &Bans{}
	err = c.getAndUnmarshal(fmt.Sprintf(BansEndpoint, user), bans)

	if !bans.Success {
		err = ErrUsernameNotFound
		return
	}
	return
}

// GetServiceBans gets bans for a user on a specific service
func (c *Client) GetServiceBans(user, service string) (bans *Bans, err error) {
	if user == "" {
		err = ErrUserRequired
		return
	}

	if service == "" {
		err = ErrServiceRequired
		return
	}

	bans = &Bans{}
	err = c.getAndUnmarshal(fmt.Sprintf(ServiceBansEndpoint, user, service), bans)

	if !bans.Success {
		err = ErrUsernameNotFound
		return
	}
	return
}
