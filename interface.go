package fishbans

// BanInfoService describes fishbans api
type BanInfoService interface {
	GetBans(user string) (bans *Bans, err error)
	GetServiceBans(user, service string) (bans *Bans, err error)

	GetBanStats(user string) (stats *BanStats, err error)
	GetServiceBanStats(user, service string) (stats *BanStats, err error)

	GetUUID(user string) (uuid string, err error)
	GetUUIDHistory(user string) (history *UUIDHistory, err error)

	ListServices() (services []*Service, err error)
}
