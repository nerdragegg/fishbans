package fishbans

// Services is returned from services endpoint
type Services struct {
	Success  bool                `json:"success"`
	Services map[string]*Service `json:"services"`
}

// Service represents data on a service
type Service struct {
	Name   string `json:"name,omitempty"`
	Legacy bool   `json:"legacy"`
	Link   string `json:"link"`
}

func getServices(data *Services) (services []*Service) {
	services = make([]*Service, len(data.Services))

	var i int
	for k, v := range data.Services {
		v.Name = k
		services[i] = v
	}

	return
}

// ListServices returns services in fishbans
func (c *Client) ListServices() (services []*Service, err error) {
	s := &Services{}
	err = c.getAndUnmarshal(ServicesEndpoint, s)
	if err != nil {
		return
	}

	if !s.Success {
		return
	}

	services = make([]*Service, len(s.Services))
	var i int
	for k, v := range s.Services {
		v.Name = k
		services[i] = v
		i++
	}
	return
}
