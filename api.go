package fishbans

import (
	"encoding/json"
	"errors"
	"net/http"
)

// UserAgent is the user agent for fishbans client library
const UserAgent = "Fishbans Go Library v1.0 https://wtfork.org/nerdrage/fishbans"

// Endpoints
const (
	BansEndpoint            = "http://api.fishbans.com/bans/%s"
	ServiceBansEndpoint     = "http://api.fishbans.com/bans/%s/%s"
	BanStatsEndpoint        = "http://api.fishbans.com/stats/%s"
	ServiceBanStatsEndpoint = "http://api.fishbans.com/stats/%s/%s"
	UUIDEndpoint            = "http://api.fishbans.com/uuid/%s"
	UUIDHistoryEndpoint     = "http://api.fishbans.com/history/%s"
	ServicesEndpoint        = "http://api.fishbans.com/services/"
)

// errors
var (
	ErrUserRequired    = errors.New("fishbans: user cannot be empty")
	ErrServiceRequired = errors.New("fishbans: service cannot be empty")
	// ErrUsernameNotFound is returned when no data is found for provided username
	ErrUsernameNotFound = errors.New("fishbans: username not found")
)

// Client implements BansInfoService
type Client struct {
	client *http.Client
}

// NewClient returns a new client
func NewClient() (client *Client) {
	return &Client{client: &http.Client{}}
}

func (c *Client) get(URL string) (resp *http.Response, err error) {
	req, err := http.NewRequest("GET", URL, nil)
	if err != nil {
		return
	}

	req.Header.Add("User-Agent", UserAgent)

	resp, err = c.client.Do(req)
	return
}

func (c *Client) getAndUnmarshal(URL string, v interface{}) (err error) {
	resp, err := c.get(URL)
	if err != nil {
		return
	}
	defer resp.Body.Close()

	err = json.NewDecoder(resp.Body).Decode(v)
	if err != nil {
		return
	}
	return
}
