package fishbans

import "fmt"

// UUID is returned from uuid endpoint request
type UUID struct {
	Success bool   `json:"success"`
	UUID    string `json:"uuid"`
}

// UUIDHistory is returned from uuid history endpoint request
type UUIDHistory struct {
	Success bool `json:"success"`
	Data    struct {
		Username string   `json:"username"`
		UUID     string   `json:"uuid"`
		History  []string `json:"history"`
	} `json:"data"`
}

// GetUUID retrieves UUID for user
func (c *Client) GetUUID(user string) (uuid string, err error) {
	if user == "" {
		err = ErrUserRequired
		return
	}

	u := &UUID{}
	err = c.getAndUnmarshal(fmt.Sprintf(UUIDEndpoint, user), u)

	if !u.Success {
		err = ErrUsernameNotFound
		return
	}

	uuid = u.UUID
	return
}

// GetUUIDHistory retrieves history of usernames for user
func (c *Client) GetUUIDHistory(user string) (history *UUIDHistory, err error) {
	if user == "" {
		err = ErrUserRequired
		return
	}

	history = &UUIDHistory{}
	err = c.getAndUnmarshal(fmt.Sprintf(UUIDHistoryEndpoint, user, history), history)
	return
}
